export function changeFn(data){
   return{
    type:'changeFn',
    data
   } 
}

export function pushCategory(value){
   return{
      type:'pushCategory',
      value
   }
}

export function loadAll(){
   return{
      type:'loadAll'
   }
}

export function searchFn(value){
   return{
      type:'searchFn',
      value
   }
}