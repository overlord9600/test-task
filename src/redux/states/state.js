import img1 from '../../images/ph1.jpg' 
import img2 from '../../images/ph2.jpg' 
import img3 from '../../images/ph3.jpg' 
import img4 from '../../images/ph4.jpg' 
import img5 from '../../images/ph5.jpg' 
import img6 from '../../images/ph6.jpg' 
import img7 from '../../images/ph7.jpg' 
import img8 from '../../images/ph8.jpg' 
import img9 from '../../images/ph9.jpg' 
import img10 from '../../images/ph10.jpg'
import img11 from '../../images/ph11.jpg'
import img12 from '../../images/ph12.jpg'
import img13 from '../../images/ph13.jpg'
import img14 from '../../images/ph14.jpg'
import img15 from '../../images/ph15.jpg'
import img16 from '../../images/ph16.jpg'


export default{
    value:'',
    error:'',
    results:[],
    search:'',
    photos:[
        {id:'forest',category:1,src:img1 ,width:3 ,height:4 },
        {id:'mashroom',category:1,src:img2 ,width:1 ,height:1 },
        {id:'river',category:1,src:img3 ,width: 3,height: 4},
        {id:'beach',category:1,src:img4 ,width: 3,height: 4},
        {id:'paper',category:1,src:img5 ,width: 3,height: 4},
        {id:'trees',category:1,src:img6 ,width: 3,height: 4},
        {id:'cristmas tree',category:2,src:img7 ,width: 3,height: 4},
        {id:'shishka',category:2,src:img8 ,width: 3,height: 4},
        {id:'road',category:2,src:img9 ,width: 3,height: 4},
        {id:'car',category:2,src:img10 ,width:3 ,height:4 },
        {id:'something else',category:2,src:img11 ,width:3 ,height:4 },
        {id:"i don't know",category:2,src:img12 ,width:3 ,height:4 },
        {id:'what',category:3,src:img13 ,width:3 ,height:4 },
        {id:'is',category:3,src:img14 ,width:3 ,height:4 },
        {id:'this',category:3,src:img15 ,width:3 ,height:4 },
        {id:'shit',category:3,src:img16 ,width:3 ,height:4 },
    ],
    showPhotos:[]
}