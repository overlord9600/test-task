import userState from '../states/state'

export default function userReducer(state=userState,action){
    let temp = {...state}

        if(action.type==='changeFn'){
            temp.value=action.data

            
            let currencies = [100,200,500,1000,2000,5000,10000,20000,50000,100000] 
             
            const splitMoney = (amount, available) => {
                const result = [];
            
                const getBills = (remainingAmount, currentStack, currentIndex) => {
                    if(!remainingAmount) {
                        result.push(currentStack);
                        return;
                    }
            
                    if(remainingAmount < 0 || remainingAmount<100 ||currentIndex >= available.length) {
                        return;
                    }
            
                    for(let i = currentIndex; i<available.length; i++) {
                        if(available[i] <= remainingAmount) {
                            let billCount = 1;
                            while(remainingAmount >= available[i]*billCount) {
                                getBills(remainingAmount - available[i]*billCount, currentStack.concat(Array(billCount).fill(available[i])), i+1)
                                billCount+=1;
                            }
                        }
                    }
                }
            
                for(let i = 0; i<available.length; i++) {
                    if(available[i] <= amount) {
                        let billCount = 1;
                        while(amount >= available[i]*billCount) {
                            getBills(amount - available[i]*billCount, Array(billCount).fill(available[i]), i+1)
                            billCount+=1;
                        }
                    }
                }
            
                return result.sort((a, b) => a.length - b.length);
            }
            let finRes=splitMoney(temp.value, currencies)
            
            temp.results = finRes.splice(1,finRes.length)
             
                  
                 

            return temp
        }

        if(action.type==='loadAll'){
            temp.showPhotos=temp.photos
            return temp           
        }

        if(action.type==='pushCategory'){
            if(action.value=='All'){
                temp.showPhotos=temp.photos
                return temp
            }else if(action.value){
                temp.showPhotos=temp.photos.filter(item=>item.category==action.value)
                return temp
            }

            console.log( temp.photos);

            return temp
        }

        if(action.type==='searchFn'){
            temp.search=action.value
            temp.showPhotos=temp.photos.filter(item=>item.id.includes(temp.search.toLocaleLowerCase()))
            return temp
        }

    return temp
}