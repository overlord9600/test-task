import React, { useState } from 'react'
import { connect } from 'react-redux';
import Test1 from './components/test1/test1';
import Test2 from './components/test2/test2';
import {BrowserRouter, Route, Link} from 'react-router-dom'
import Header from './components/Header';
function App(){
  return (
    <div className="App">
      <BrowserRouter>
          <Header/>
          <Route exact path='/test1' component={Test1}/>
          <Route exact path='/test2' component={Test2}/>
      </BrowserRouter>
     </div>
  );
}

export default connect(r=>r)(App);
