import { Grid, Input, makeStyles } from '@material-ui/core'
import React, { useState } from 'react'
import { connect } from 'react-redux';
import '../../App.css'
import { changeFn } from '../../redux/action';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }
}));




function FirstTest(props) {
  const [currentPage, setCurrentPage]= useState(1)
  const [itemsPerPage]=useState(3)
  const pageNumbers=[]

  for (let i=1;i<=Math.ceil(props.value.results.length/itemsPerPage);i++){
    pageNumbers.push(i)
  }

  const indexOfLastItem = currentPage * itemsPerPage
  const indexOfFirstItem = indexOfLastItem - itemsPerPage

  const currentItems = props.value.results.slice(indexOfFirstItem,indexOfLastItem)
  
  const classes = useStyles();
  return (
    <div className="App">
       <Input 
       style={{width:'70%',margin:'50px'}}
       onChange={(e)=>{props.dispatch(changeFn(e.target.value))}}
       value={props.value.value}
       placeholder='Enter the currency...'
       />
       <div className={classes.root}>
        <Grid container>
            <Grid item xs={12}>              
              {
                 currentItems.map((item, index)=>{
                   return(
                     <div key={index} className='res'>
                      <p>{JSON.stringify(item).substring(1, JSON.stringify(item).length-1)}</p> 
                     </div>
                   )
                 })
               }
               <nav>
                  <ul  className='pagination'>
                    {
                      pageNumbers.map(numbers=>{
                       return <li key={numbers}>
                          <a onClick={()=>{setCurrentPage(numbers)}}>{numbers}</a>
                        </li>
                      })
                    }
                  </ul>
               </nav>
            </Grid>
         </Grid>
       </div>
    </div>
  );
}

export default connect(r=>r)(FirstTest);
