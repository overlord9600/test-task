import React from 'react';
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import './header.css'
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }));

export default function Header(){
    const classes = useStyles();
    return(
        <AppBar position="static">
  <Toolbar>
     
    <Typography variant="h6" className={classes.title}>
    <Link to='test1'>Algorithm test</Link>
    <Link to='test2'>Gallery filter test</Link>
    </Typography>
    
  </Toolbar>
</AppBar>
        // <div className='test-nav'>
        //     <Link to='/test1'>Algorithm test</Link>
        //     <Link to='/test2'>Gallery filter test</Link>
        // </div>
    )
}