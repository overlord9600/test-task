import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import './photo.css'
const Checkmark = ({text}) => {
    // useEffect(()=>{

    // },[text])
     
    console.log(text);
  return(
        <div style={ {  top: "25%", right:'15%',left:'15%', position: "absolute", zIndex: "1" } }>
         <div className='caption'>
             {text}
          </div>
        </div>
        )
};


 
 
const cont = {
  position: "relative"
};

const SelectedImage = ({
  index,
  photo,
  margin,
  direction,
  top,
  left,
  
}) => {
//    console.log(photo);
//    console.log(text);

  return (
    <div
      style={{ margin, height: photo.height, width: photo.width, ...cont }}
     >
      <Checkmark text={photo.id}/>
      <img
        alt={photo.title}
        
        {...photo}
       />
     </div>
  );
};

export default connect(r=>r)(SelectedImage);
