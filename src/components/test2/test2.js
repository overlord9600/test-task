import React, { useCallback, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Gallery from 'react-photo-gallery'
import { loadAll, pushCategory,searchFn } from '../../redux/action';
import { Button, Input } from '@material-ui/core';
import './test2.css'
import SelectedImage from './imgCaption';


const GalleryTest = (props) => {
// console.log(props);
    useEffect(()=>{
        props.dispatch(loadAll())
    },[])

 

  const imageRenderer = useCallback(
    ({ index, left, top, key, photo }) => (
      <SelectedImage
        key={key}
        margin={"2px"}
        index={index}
        photo={photo}
        left={left}
        top={top}
       />
    ),[]
  );

    return (
        <div className='gallery'>
            <div className='categories'>
                <Button variant="contained" onClick={()=>{props.dispatch(pushCategory('All'))}}>Show all</Button>
                <Button variant="contained" onClick={()=>{props.dispatch(pushCategory('1'))}}>Category 1</Button>
                <Button variant="contained" onClick={()=>{props.dispatch(pushCategory('2'))}}>Category 2</Button>
                <Button variant="contained" onClick={()=>{props.dispatch(pushCategory('3'))}}>Category 3</Button>
             </div>
             <div className='inp'>
                <Input
                placeholder='type 1-16 to see the result...'
                value={props.value.search}
                onChange={(e)=>{props.dispatch(searchFn(e.target.value))}}/>
             </div>
            
            <Gallery photos={props.value.showPhotos} renderImage={imageRenderer} direction={"row"} />
        </div>
    );
}
 
export default connect(r=>r)(GalleryTest);